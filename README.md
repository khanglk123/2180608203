|       Title    		|As a bookseller, i want my store have many type of books, so that customers can comfortably choose books              
|------------------------|-------------------------------|
|Acceptance Criteria	|1.Consider the book genres that customers want to purchase for importing .<br> 2.Research popular book genres, stay current with current trends, and understand the common preferences of each age group in order to stock books.<br>3.Finding a reputable book supplier as well as identifying one's main readership group.             |
|Definition of Done         |1.Acquiring a specific customer base.<br>2.Understand the customer's needs.<br>3.Easily change the type of books according to trends. <br>4. Focus on the user’s needs and goals.|
|Owner          |Tran Thanh Tuong|
|Iteration  |Unscheduled    |
|Estimate   |5 Points   |